package xyz.ionics.conscra.exceptions;

import org.apache.logging.log4j.util.Strings;

public class StreetAddressFormatException extends Exception {

    public StreetAddressFormatException() {
        super(Strings.EMPTY);
    }

    public StreetAddressFormatException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        if(super.getMessage().equals(Strings.EMPTY))
            return "Street address could not be decoded into street and building number.";
        return super.getMessage();
    }

}
