package xyz.ionics.conscra.exceptions;

import java.io.IOException;
import java.net.URL;

/**
 * Signals that http request failed due to a connectivity problem, timeout or cancellation.
 *
 * @author Gioele De Vitti
 */
public class HttpRequestException extends IOException {

    private final URL url;

    public HttpRequestException(URL url, Throwable cause) {
        super(cause);
        this.url = url;
    }

    @Override
    public String getMessage() {
        return "URL: " + url + "\n" + super.getMessage();
    }

}
