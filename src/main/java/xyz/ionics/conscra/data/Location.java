package xyz.ionics.conscra.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.ionics.conscra.exceptions.StreetAddressFormatException;
import xyz.ionics.conscra.util.StringUtil;

import java.util.regex.Pattern;

public class Location {

    private static final Logger logger = LogManager.getLogger();

    private String street, buildingNumber;
    private final String postalCode, city;

    public Location(String streetAddress, String postalCode, String city) throws StreetAddressFormatException {
        decodeStreetAddress(streetAddress);
        this.postalCode = postalCode;
        this.city = city;
    }

    private void decodeStreetAddress(String streetAddress) throws StreetAddressFormatException {
        // Remove non-alphanumeric characters, except for whitespaces
        streetAddress = streetAddress.replaceAll("[^A-Za-z\\d ]", "");
        // Split by whitespaces
        String[] parts = streetAddress.split(" ");

        Pattern patternBuildingNumber = Pattern.compile(".*\\d+.*");
        int srcPos;
        // If the building number is at the beginning or at the end
        if (patternBuildingNumber.matcher(parts[0]).matches()) {
            buildingNumber = parts[0];
            srcPos = 1;
        } else if (patternBuildingNumber.matcher(parts[parts.length - 1]).matches()) {
            buildingNumber = parts[parts.length - 1];
            srcPos = 0;
        } else
            throw new StreetAddressFormatException();

        String[] streetParts = new String[parts.length - 1];
        System.arraycopy(parts, srcPos, streetParts, 0, streetParts.length);
        street = StringUtil.concat(" ", streetParts);
    }

    public Location(String street, String buildingNumber, String postalCode, String city) {
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public Integer getBuildingNumberInteger() {
        // Remove non-numeric characters.
        String sanitized = buildingNumber.replaceAll("[^\\d]", "");
        int number = -1;
        try {
            number = Integer.parseInt(sanitized);
        } catch (NumberFormatException e) {
            logger.warn("Wrongly formatted building number for location:\n {}", this);
        }
        return number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return street + " " + buildingNumber + " - " + postalCode + " " + city;
    }

}
