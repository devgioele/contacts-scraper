package xyz.ionics.conscra.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class NumberValidators<T extends Number> extends ArrayList<NumberValidator<T>> {

    public NumberValidators(Collection<NumberValidator<T>> validators) {
        super(validators);
    }

    public NumberValidators(List<NumberValidator<T>> validators) {
        super(validators);
    }

    public NumberValidators(NumberValidator<T>... validators) {
        super(Arrays.asList(validators));
    }

    public boolean isValid(T n) {
        for(NumberValidator<T> validator : this)
            if(!validator.isValid(n))
                return false;
        return true;
    }

}
