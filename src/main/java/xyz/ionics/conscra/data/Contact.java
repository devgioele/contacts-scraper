package xyz.ionics.conscra.data;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class Contact implements Comparable<Contact> {

    private final String name;
    private final Set<String> telephones;
    private final Location location;

    public Contact(String name, Set<String> telephones, Location location) {
        this.name = name;
        this.telephones = telephones;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public Set<String> getTelephones() {
        return telephones;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(name + "\n" + location + "\n");
        int i = 0;

        for (String telephone : telephones) {
            result.append(telephone);
            if (i < telephones.size() - 1)
                result.append("\n");
            i++;
        }

        return result.toString();
    }

    /**
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(@NotNull Contact o) {
        return location.getBuildingNumberInteger().compareTo(o.location.getBuildingNumberInteger());
    }

}
