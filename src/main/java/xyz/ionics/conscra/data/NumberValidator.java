package xyz.ionics.conscra.data;

/**
 * Functional interface.
 */
public interface NumberValidator<T extends Number> {

    boolean isValid(T n);

}
