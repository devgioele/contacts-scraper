package xyz.ionics.conscra.data;

import java.util.Arrays;
import java.util.Collections;

public class ContactsParam {

    private final String streetAddress;
    private final NumberValidators<Integer> validatorsBuildingNumber;

    @SafeVarargs
    public ContactsParam(String streetAddress, NumberValidator<Integer>... validatorsBuildingNumber) {
        this.streetAddress = streetAddress;
        this.validatorsBuildingNumber = new NumberValidators<>(validatorsBuildingNumber);
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public NumberValidators<Integer> getValidatorsBuildingNumber() {
        return validatorsBuildingNumber;
    }

}
