package xyz.ionics.conscra.util;

public class StringUtil {

    public static boolean isUppercase(String str) {
        char[] arr = str.toCharArray();
        for (Character c : arr)
            if (c >= 'a' && c <= 'z')
                return false;
        return true;
    }

    public static String concat(String delimiter, String[] list) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < list.length - 1; i++)
            builder.append(list[i]).append(delimiter);
        builder.append(list[list.length - 1]);

        return builder.toString();
    }

    public static String toFirstUpperCase(String name) {
        StringBuilder result = new StringBuilder();

        String[] parts = name.split(" ");
        for (String part : parts) {
            result.append(part.substring(0, 1).toUpperCase());
            result.append(part.substring(1).toLowerCase());
            result.append(" ");
        }

        return result.toString();
    }

}
