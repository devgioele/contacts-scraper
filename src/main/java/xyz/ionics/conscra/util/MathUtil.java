package xyz.ionics.conscra.util;

public class MathUtil {

    @SafeVarargs
    public static <T extends Comparable<T>> T max(T... values) {
        T max = values[0];
        for (int i = 1; i < values.length; i++)
            if (values[i].compareTo(max) > 0)
                max = values[i];
        return max;
    }

    @SafeVarargs
    public static <T extends Comparable<T>> T min(T... values) {
        T min = values[0];
        for (int i = 1; i < values.length; i++)
            if (values[i].compareTo(min) < 0)
                min = values[i];
        return min;
    }

}
