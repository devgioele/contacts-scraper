package xyz.ionics.conscra.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.ionics.conscra.data.ContactsParam;
import xyz.ionics.conscra.exceptions.UncaughtExceptionHandler;
import xyz.ionics.conscra.extraction.ContactsScraper;
import xyz.ionics.conscra.extraction.PagineBiancheScraper;
import xyz.ionics.conscra.extraction.PagineGialleScraper;
import xyz.ionics.conscra.ui.UI;

import java.util.LinkedList;
import java.util.List;

public class App {

    public static String workingDirectory;

    private static final Logger logger;
    static {
        System.setProperty("WORKING_DIR_CONTACTS_SCRAPER", getWorkingDirectory());
        logger = LogManager.getLogger();
    }

    public static void main(String[] args) {
        boot();

        ContactsParam param = UI.getInstance().receiveContactsParam();

        logger.info("Working.");
        List<ContactsScraper> scrapers = new LinkedList<>();
        scrapers.add(new PagineBiancheScraper(param));
        scrapers.add(new PagineGialleScraper(param));
        scrapers.parallelStream()
                .forEach(scraper -> UI.getInstance().send(scraper, scraper.searchContacts()));

        UI.getInstance().close();
        shutdown(0);
    }

    public static void boot() {
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler());
    }

    public static void shutdown(int status) {
        System.exit(status);
    }

    private static String getWorkingDirectory() {
        if (workingDirectory == null) {
            String os = (System.getProperty("os.name")).toUpperCase();
            // Windows
            if (os.contains("WIN"))
                workingDirectory = System.getenv("AppData");
            else // Mac or Linux
                workingDirectory = System.getProperty("user.home");
            workingDirectory += "/Contacts Scraper";
        }
        return workingDirectory;
    }

}
