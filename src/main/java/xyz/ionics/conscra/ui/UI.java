package xyz.ionics.conscra.ui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.ionics.conscra.data.Contact;
import xyz.ionics.conscra.data.ContactsParam;
import xyz.ionics.conscra.data.NumberValidator;
import xyz.ionics.conscra.extraction.ContactsScraper;
import xyz.ionics.conscra.ui.interpreters.*;

import java.util.Collection;
import java.util.Scanner;

public class UI {

    private enum Message {
        CityStreet("Which city and which street?"),
        BuildingNumberEvenOdd("Which constraint should be applied to building numbers?\n" +
                "[e] only even numbers.\n" +
                "[o] only odd numbers.\n" +
                "[b] both."),
        BuildingNumberMin("Minimum value of building numbers? Leave empty to not apply a minimum."),
        BuildingNumberMax("Maximum value of building numbers? Leave empty to not apply a maximum.");

        private final String message;

        Message(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return message;
        }

    }
    private static final Logger logger = LogManager.getLogger();

    private static UI INSTANCE;

    private final Scanner sc;

    private UI() {
        sc = new Scanner(System.in);
    }

    public static UI getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UI();
        }
        return INSTANCE;
    }

    public ContactsParam receiveContactsParam() {
        // Get street address
        String streetAddress = receive(Message.CityStreet, new SimpleStringInterpreter());

        // Get first building number validator
        NumberValidator<Integer> validatorEvenOdd = receive(Message.BuildingNumberEvenOdd, new EvenOddInterpreter());

        // Get building number validator for minimum
        NumberValidator<Integer> validatorMin = receive(Message.BuildingNumberMin, new MinInterpreter(0));

        // Get building number validator for maximum
        NumberValidator<Integer> validatorMax = receive(Message.BuildingNumberMax, new MaxInterpreter());

        // Package
        return new ContactsParam(streetAddress, validatorEvenOdd, validatorMin, validatorMax);
    }

    private <T> T receive(Message question, Interpreter<T> interpreter) {
        T result;

        logger.info(question);
        do {
            String in = sc.nextLine();
            result = interpreter.resolve(in);
            if (result == null)
                logger.info("Invalid command.");
        } while (result == null);

        return result;
    }

    public void send(ContactsScraper source, Collection<Contact> contacts) {
        logger.info("--------------------");
        logger.info("Contacts found on {}: {}", source.getSourceName(), contacts.size());
        logger.info("");
        for (Contact c : contacts) {
            logger.info(c);
            logger.info("");
        }
    }

    public void close() {
        logger.info("Press [enter] to quit.");
        sc.nextLine();
    }

}
