package xyz.ionics.conscra.ui.interpreters;

import xyz.ionics.conscra.data.NumberValidator;

public class MinInterpreter implements Interpreter<NumberValidator<Integer>> {

    private final Integer defaultValue;

    public MinInterpreter() {
        defaultValue = Integer.MIN_VALUE;
    }

    public MinInterpreter(Integer defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Processes the input string to output the corresponding object.
     *
     * @param line The line that the user entered through the console.
     * @return The object that corresponds to the input. Null if the input is invalid.
     */
    @Override
    public NumberValidator<Integer> resolve(String line) {
        int min;

        if (line.isEmpty())
            min = defaultValue;
        else {
            try {
                min = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        return (n) -> n >= min;
    }

}
