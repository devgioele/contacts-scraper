package xyz.ionics.conscra.ui.interpreters;

public class SimpleStringInterpreter implements Interpreter<String> {

    /**
     * Processes the input string to output the corresponding object.
     *
     * @param line The line that the user entered through the console.
     * @return The object that corresponds to the input. Null if the input is invalid.
     */
    @Override
    public String resolve(String line) {
        return line;
    }

}
