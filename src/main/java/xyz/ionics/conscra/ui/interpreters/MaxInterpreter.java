package xyz.ionics.conscra.ui.interpreters;

import xyz.ionics.conscra.data.NumberValidator;

public class MaxInterpreter implements Interpreter<NumberValidator<Integer>> {

    private final Integer defaultValue;

    public MaxInterpreter() {
        defaultValue = Integer.MAX_VALUE;
    }

    public MaxInterpreter(Integer defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Processes the input string to output the corresponding object.
     *
     * @param line The line that the user entered through the console.
     * @return The object that corresponds to the input. Null if the input is invalid.
     */
    @Override
    public NumberValidator<Integer> resolve(String line) {
        int max;

        if (line.isEmpty())
            max = defaultValue;
        else {
            try {
                max = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        return (n) -> n <= max;
    }

}
