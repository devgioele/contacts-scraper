package xyz.ionics.conscra.ui.interpreters;

import xyz.ionics.conscra.data.NumberValidator;

public class EvenOddInterpreter implements Interpreter<NumberValidator<Integer>> {


    /**
     * Processes the input string to output the corresponding object.
     *
     * @param line The line that the user entered through the console.
     * @return The object that corresponds to the input. Null if the input is invalid.
     */
    @Override
    public NumberValidator<Integer> resolve(String line) {
        return switch (line) {
            case "e" -> (n) -> n % 2 == 0;
            case "o" -> (n) -> n % 2 == 1;
            case "b" -> (n) -> true;
            default -> null;
        };
    }

}
