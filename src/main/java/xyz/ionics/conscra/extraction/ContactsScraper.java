package xyz.ionics.conscra.extraction;

import kotlin.text.Charsets;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.ionics.conscra.data.Contact;
import xyz.ionics.conscra.data.ContactsParam;
import xyz.ionics.conscra.exceptions.HttpRequestException;

import java.io.IOException;
import java.util.Set;

public abstract class ContactsScraper {

    private static final Logger logger = LogManager.getLogger();

    private final OkHttpClient httpClient = new OkHttpClient();

    private final String sourceName;
    protected final ContactsParam param;

    public ContactsScraper(String sourceName, ContactsParam param) {
        this.sourceName = sourceName;
        this.param = param;
    }

    public String getSourceName() {
        return sourceName;
    }

    public abstract Set<Contact> searchContacts();

    protected abstract ScrapeResult scrapeFrom(String html);

    protected String request(HttpUrl url) throws HttpRequestException {
        Call call = httpClient.newCall(new Request.Builder().url(url).build());
        String html = null;

        logger.debug("Invoking request to {}", url.toString());
        try (ResponseBody body = call.execute().body()) {
            if (body != null) {
                logger.debug("Receiving and decoding.");
                // This takes a couple of seconds!
                html = body.source().readString(Charsets.UTF_8);
            }
        } catch (IOException e) {
            throw new HttpRequestException(url.url(), e);
        }

        return html;
    }

}
