package xyz.ionics.conscra.extraction;

import okhttp3.HttpUrl;
import xyz.ionics.conscra.data.Contact;

import java.util.Set;

public class ScrapeResult {

    private final Set<Contact> contacts;
    private final HttpUrl nextPage;

    public ScrapeResult(Set<Contact> contacts, HttpUrl nextPage) {
        this.contacts = contacts;
        this.nextPage = nextPage;
    }

    public Set<Contact> contacts() {
        return contacts;
    }

    public HttpUrl nextPage() {
        return nextPage;
    }

}
