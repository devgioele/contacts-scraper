package xyz.ionics.conscra.extraction;

import okhttp3.HttpUrl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import xyz.ionics.conscra.data.Contact;
import xyz.ionics.conscra.data.ContactsParam;
import xyz.ionics.conscra.data.Location;
import xyz.ionics.conscra.exceptions.ElementNotFoundException;
import xyz.ionics.conscra.exceptions.HttpRequestException;
import xyz.ionics.conscra.exceptions.StreetAddressFormatException;
import xyz.ionics.conscra.util.StringUtil;

import java.util.Set;
import java.util.TreeSet;

public class PagineBiancheScraper extends ContactsScraper {

    private static final Logger logger = LogManager.getLogger();

    private final HttpUrl startPage;

    public PagineBiancheScraper(ContactsParam param) {
        super("Pagine Bianche", param);
        startPage = new HttpUrl.Builder()
                .scheme("https")
                .host("www.paginebianche.it")
                .addPathSegment("cerca-da-indirizzo")
                .addQueryParameter("dv", param.getStreetAddress())
                .build();
    }

    public Set<Contact> searchContacts() {
        Set<Contact> contacts = new TreeSet<>();
        try {
            // Visit all pages containing contacts, one after another.
            HttpUrl nextPage = startPage;
            while (nextPage != null) {
                String html = request(nextPage);
                ScrapeResult result = scrapeFrom(html);
                contacts.addAll(result.contacts());
                nextPage = result.nextPage();
            }
        } catch (HttpRequestException e) {
            logger.warn("Communication failed with " + getSourceName() + ". {}", e.getMessage());
        }

        return contacts;
    }

    protected ScrapeResult scrapeFrom(String html) {
        Document doc = Jsoup.parse(html);

        Set<Contact> contacts = extractContacts(doc);

        // Get next url, if there is any
        HttpUrl nextPage = searchNextPage(doc);

        return new ScrapeResult(contacts, nextPage);
    }

    private Set<Contact> extractContacts(Document doc) {
        Set<Contact> contacts = new TreeSet<>();

        for (Element contact : doc.select("div[class=\"item_sx vcard\"]")) {
            try {
                String name = extractName(contact);
                Location loc = extractLocation(contact);
                if (!param.getValidatorsBuildingNumber().isValid(loc.getBuildingNumberInteger()))
                    continue;
                Set<String> telephones = extractTelephones(contact);
                contacts.add(new Contact(name, telephones, loc));
            } catch (ElementNotFoundException | StreetAddressFormatException ignored) {
            }
        }

        return contacts;
    }

    private String extractName(Element contact) throws ElementNotFoundException {
        Element sub1 = contact.selectFirst("div[class=\"item_head\"]");
        if (sub1 == null) throw new ElementNotFoundException();
        Element sub2 = sub1.selectFirst("a[title]");
        if (sub2 == null) throw new ElementNotFoundException();
        return StringUtil.toFirstUpperCase(sub2.text());
    }

    private Location extractLocation(Element contact) throws ElementNotFoundException, StreetAddressFormatException {
        Element addressRoot = contact.selectFirst("span[itemprop=\"address\"]");
        if (addressRoot == null) throw new ElementNotFoundException();

        Element streetAddressElement = addressRoot.selectFirst("span[itemprop=\"streetAddress\"]");
        if (streetAddressElement == null) throw new ElementNotFoundException();
        String streetAddress = streetAddressElement.text();

        Element postalCodeElement = addressRoot.selectFirst("span[itemprop=\"postalCode\"]");
        if (postalCodeElement == null) throw new ElementNotFoundException();
        String postalCode = postalCodeElement.text();

        Element cityElement = addressRoot.selectFirst("span[itemprop=\"addressLocality\"]");
        if (cityElement == null) throw new ElementNotFoundException();
        String city = cityElement.text();

        return new Location(streetAddress, postalCode, city);
    }

    private Set<String> extractTelephones(Element contact) {
        Set<String> tel = new TreeSet<>();

        Elements telElements = contact.select("span[itemprop=\"telephone\"]");
        for (Element telElement : telElements)
            tel.add(telElement.text());

        return tel;
    }

    private HttpUrl searchNextPage(Document doc) {
        logger.debug("Searching for url of next page.");

        Element e = doc.select("ul[class=\"listing-pag\"]").first();

        if (e != null) {
            String nextUrl = e.select("a[class=\"listing-pag-n listing-pag-succ\"]")
                    .attr("href");

            if (!nextUrl.isEmpty()) {
                logger.debug("Url of next page: {}.", nextUrl);
                return HttpUrl.parse(nextUrl);
            }
        }

        logger.debug("No successive page found.");
        return null;
    }

}

